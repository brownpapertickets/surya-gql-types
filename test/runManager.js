import { TypesManager } from "../src/typesManager"
import { MockLog } from "./mockLog"
import { MockDB } from "./apis/mockDB"

class TestTool {
  async run() {
    const log = new MockLog()
    let container = { log }
    const mockDB = new MockDB(container)
    container.dataInterfaces = { MockDB: mockDB }

    log.info(`>>> Run Surya tests`)
    const typesManager = new TypesManager(container)

    log.info("LoadTypesModules")
    await typesManager.loadTypeModules(__dirname + "/types")

    log.info("Connect Databases")
    const dbx = await mockDB.connect("dbx:user@password:servername", false)

    log.info(`POST Test ===============================================`)
    const typeDefs = typesManager.getTypeDefs()

    log.info(`TypeDefs: ${typeDefs}`)
    log.info(
      `-----------------------\nResolvers: ${JSON.stringify(
        typesManager.getResolvers(),
        null,
        2
      )}`
    )
    log.info(
      `-----------------------\nManagers: ${JSON.stringify(
        Object.keys(typesManager.getManagers(), null, 2)
      )}`
    )
  }
}

const test = new TestTool()
test.run()
