import { BaseCRUDManager } from "../../../src/baseCRUDManager"

const authorized = {
  get: { groups: ["*"] },
  query: { groups: ["*"] },
  create: { groups: ["*"] },
  update: { groups: ["admin"], owner: true },
  delete: { groups: ["admin"] },
}

export class EventManager extends BaseCRUDManager {
  authorized() {
    return authorized
  }

  async create(data, context, options) {
    data.postedDate = new Date().toISOString()
    return super.create(data, context, options)
  }
}
