export const EventResolvers = {
  Query: {
    events: async (_, args, context) => {
      return await context.managers
        .get("Event")
        .query(args, context, { doAuth: true })
    },
    event: async (_, args, context) => {
      return await context.managers
        .get("Event")
        .get(args, context, { doAuth: true })
    },
  },
  Mutation: {
    createEvent: async (_, args, context) => {
      return await context.managers
        .get("Event")
        .create(args.event, context, { doAuth: true })
    },
    createEventVenue: async (_, args, context) => {
      const venueResult = await context.managers
        .get("Venue")
        .create(args.event.venue, context, { doAuth: true })
      const venueId = venueResult.data.id
      if (venueId) {
        args.event.venueId = venueId
        return await context.managers
          .get("Event")
          .create(args.event, context, { doAuth: true })
      } else {
        return { success: false, message: "Error creating Venue", data: null }
      }
    },

    updateEvent: async (_, args, context) => {
      return await context.managers
        .get("Event")
        .update(args.id, args.event, context, {
          doAuth: true,
        })
    },

    deleteEvent: async (_, args, context) => {
      return await context.managers
        .get("Event")
        .delete(args.id, context, { doAuth: true })
    },
  },
  Event: {
    venue: async (obj, __, context) => {
      return await context.managers
        .get("Venue")
        .getLinked({ obj: obj, byField: "venueId" }, context, { doAuth: true })
    },
    owner: async (obj, __, context) => {
      return await context.managers
        .get("User")
        .getLinked({ obj: obj, byField: "ownerId" }, context, { doAuth: true })
    },
  },
}
