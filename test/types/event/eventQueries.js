export const EventQueries = `
  extend type Query {
    events(
      offset: Int
      limit: Int
      filter: String
      sort: String
    ): EventSetResponse
    event(id: ID!): Event
  }

  extend type Mutation {
    createEvent(event: EventInput!): EventMutationResponse!
    updateEvent(id: ID!, event: EventInput!): EventMutationResponse!
    deleteEvent(id: ID!): EventMutationResponse!
  }
`
