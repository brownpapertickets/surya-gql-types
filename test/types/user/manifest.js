export const Manifest = {
  sysVersion: "1.0.0",
  typeVersion: "0.1.2",
  description: "MongoDB Based Users",
  types: ["User", "AuthToken", "Credentials", "Address"],
  queries: "UserQueries",
  resolvers: "UserResolvers",
  manager: "UserManager",
  data: { interface: "MockDB", api: "UserAPI" },
}
