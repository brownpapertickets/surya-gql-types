import { GraphQLScalarType } from "graphql"

export const DemoScalar = {
  DemoScalar: new GraphQLScalarType({
    name: "DemoScalar",
    description: "A basic framework for a scalar",
    serialize(value) {
      let result
      // Implement your own behavior here by setting the 'result' variable
      return result
    },
    parseValue(value) {
      let result
      // Implement your own behavior here by setting the 'result' variable
      return result
    },
    parseLiteral(ast) {
      switch (
        ast.kind
        // Implement your own behavior here by returning what suits your needs
        // depending on ast.kind
      ) {
      }
    },
  }),
}
