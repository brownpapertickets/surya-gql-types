import { gql } from "apollo-server"

export const CommonGQL = gql`
  enum RGB {
    RED
    GREEN
    BLUE
  }

  scalar DemoScalar
  scalar EmailScalar
`
