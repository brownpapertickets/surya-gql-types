export const Manifest = {
  sysVersion: "1.0.0",
  typeVersion: "0.1.1",
  name: "Common",
  description:
    "Generic Types, Enumerations (and value resolvers), and common custom Scalars",
  types: ["Address"],
  queries: "CommonGQL",
  resolvers: ["DemoScalar", "EmailScalar"],
}
