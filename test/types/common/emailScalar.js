import { GraphQLScalarType } from "graphql"
import { Kind } from "graphql"

export const EmailScalar = {
  EmailScalar: new GraphQLScalarType({
    name: "EmailScalar",
    description: "A validated string matching for email",
    serialize(value) {
      console.log(`Email.serialize(${value})`)
      let result
      // Implement your own behavior here by setting the 'result' variable
      return result
    },
    parseValue(value) {
      console.log(`Email.parseValue(${value})`)
      let result
      // Implement your own behavior here by setting the 'result' variable
      return result
    },
    parseLiteral(ast) {
      console.log(`Email.parseSerial(${JSON.stringify(ast)})`)
      switch (ast.kind) {
        case Kind.INT:
        case Kind.STRING:
        // Implement your own behavior here by returning what suits your needs
        // depending on ast.kind
      }
    },
  }),
}
