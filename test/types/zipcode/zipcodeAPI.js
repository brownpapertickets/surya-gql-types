import { BasePGSqlAPI } from "@brownpapertickets/surya-gql-data-pgsql"

export class ZipcodeAPI extends BasePGSqlAPI {
  constructor(container, type, typeMap, config) {
    super(container, type, typeMap, config)
    this.loadSymbolics()
  }

  loadSymbolics() {
    const filterDistanceMiles = `SELECT sz.coordinates <@> point ($1$, $2$) AS distanceMiles FROM zipcodes AS sz
    WHERE sz.zipcode_id = $1`

    this.setSymbolic("distanceMiles", filterDistanceMiles)
  }
}
