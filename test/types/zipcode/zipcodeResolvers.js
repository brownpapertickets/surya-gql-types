export const ZipcodeResolvers = {
  Query: {
    zipcodes: async (_, args, context) => {
      return await context.managers
        .get("Zipcode")
        .query(args, context, { doAuth: true })
    },
  },
}
