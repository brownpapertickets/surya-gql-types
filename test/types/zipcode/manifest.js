export const Manifest = {
  sysVersion: "1.0.0",
  typeVersion: "1.0.0",
  description: "Zipcode Data - source from Maxmind etc.",
  types: ["Zipcode", "Coordinates"],
  queries: "ZipcodeQueries",
  resolvers: "ZipcodeResolvers",
  manager: "ZipcodeManager",
  data: {
    interface: "PGSql",
    api: "ZipcodeAPI",
    config: { table: "zipcodes", getTotal: false },
  },
}
