import { BaseManager } from "../../../src/baseManager"

const authorized = {
  get: { groups: ["*"] },
  query: { groups: ["*"] },
  create: { groups: ["admin"] },
  update: { groups: ["admin"], owner: true },
  delete: { groups: ["admin"] },
}

export class SimpleManager extends BaseManager {
  authorized() {
    return authorized
  }

  async doSimpleThing(data, { auth }, { doAuth }) {
    await this.authorize("admin", doAuth, null, auth)
    this.log.info(`doSimpleThing: ${JSON.stringify(data)}`)
  }
}
