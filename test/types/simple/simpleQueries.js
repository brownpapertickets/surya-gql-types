export const SimpleQueries = `
  extend type Query {
    simples(
      offset: Int
      limit: Int
      filter: String
      sort: String
    ): SimpleSetResponse
    simple(id: ID!): Simple
  }

  extend type Mutation {
    createSimple(simple: SimpleInput!): SimpleMutationResponse
    updateSimple(id: ID!, event: SimpleInput!): SimpleMutationResponse!
    deleteSimple(id: ID!): SimpleMutationResponse!
  }
`
