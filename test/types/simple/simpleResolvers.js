export const SimpleResolvers = {
  Query: {
    simples: async (_, args, context) => {
      return await context.managers
        .get("Simple")
        .query(args, context, { doAuth: true })
    },
    simple: async (_, args, context) => {
      return await context.managers
        .get("Simple")
        .get(args, context, { doAuth: true })
    },
  },
  Mutation: {
    createSimple: async (_, args, context) => {
      return await context.managers
        .get("Simple")
        .create(args.event, context, { doAuth: true })
    },
    updateSimple: async (_, args, context) => {
      return await context.managers
        .get("Simple")
        .update(args.event, context, { doAuth: true })
    },
    deleteEvent: async (_, args, context) => {
      return await context.managers
        .get("Simple")
        .delete(args.id, context, { doAuth: true })
    },
  },
}
