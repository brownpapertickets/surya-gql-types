/**
 * A simple class to enclose the map of managers by typeName
 * with a get function
 */

export class ManagersMap {
  constructor(managers) {
    this.managers = managers
  }

  get(typeName) {
    return this.managers[typeName] || null
  }
}
