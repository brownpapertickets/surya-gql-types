import path from "path"
import fs from "fs"
import { merge, concat } from "lodash"
import { isFunction, isString } from "util"
import { gql } from "apollo-server"

import { Scalars, DateScalar } from "@brownpapertickets/surya-gql-scalar"

import { ManagersMap } from "./managersMap"

const manifestName = "Manifest"
const queriesSuffix = "Queries"
const resolversSuffux = "Resolvers"
const managerSuffix = "Manager"

const emptyQuery = gql`
  type Query {
    _empty: String
  }

  type Mutation {
    _empty: String
  }

  type Subscription {
    _empty: String
  }
`
const emptyResolvers = {
  Query: {},
}

export class TypesManager {
  constructor(container) {
    this.container = container
    this.log = container.log
    this.dataInterfaces = container.dataInterfaces

    this.types = {}
    this.schemas = []
    this.queries = [emptyQuery, Scalars]
    this.resolvers = merge(emptyResolvers, DateScalar)
    this.managers = {}
    this.interfaceMap = {}
  }

  async loadTypeModules(basePath, typesOnly = false) {
    return new Promise((resolve, reject) => {
      fs.readdir(basePath, async (err, items) => {
        if (err) {
          reject(err)
        } else {
          items.forEach((item) => {
            const itemPath = path.join(basePath, item)
            const isDir = fs.lstatSync(itemPath).isDirectory()
            if (isDir) {
              try {
                this.loadTypeModule(item, itemPath, reject, typesOnly)
              } catch (ex) {
                this.log.error(`Type module "${item}" failed to load: ${ex}`)
              }
            }
          })

          if (!typesOnly) {
            try {
              await this.createAPIs()
            } catch (ex) {
              this.log.error(`Error creating APIs ${ex.message}`)
              reject(ex)
            }
          }
        }

        resolve(true)
      })
    })
  }

  loadTypeModule(moduleName, modulePath, reject, typesOnly) {
    const typeModule = require(modulePath)
    let typeModuleItemNames = Object.keys(typeModule)
    if (typeModuleItemNames.includes(manifestName)) {
      const manifest = typeModule.Manifest
      typeModuleItemNames = this.arrayRemove(typeModuleItemNames, manifestName)

      const types = manifest.types || []
      let baseTypeName = this.firstUpperCase(moduleName)
      if (types.length > 0) {
        baseTypeName = types[0]
      }

      if (!typesOnly) {
        //load Queries
        typeModuleItemNames = this.loadQueries(
          baseTypeName,
          manifest,
          typeModule,
          typeModuleItemNames
        )
        // load Resolvers
        typeModuleItemNames = this.loadResolvers(
          baseTypeName,
          manifest,
          typeModule,
          typeModuleItemNames
        )
        // load Manager
        try {
          typeModuleItemNames = this.loadManager(
            baseTypeName,
            manifest,
            typeModule,
            typeModuleItemNames
          )
        } catch (ex) {
          this.log.error(`Error loading Manager ${ex.message}`)
          reject(ex)
          return
        }
        // capture API info
        typeModuleItemNames = this.loadAPI(
          baseTypeName,
          manifest,
          typeModule,
          typeModuleItemNames
        )
      }
      // load all Types
      try {
        this.loadTypes(baseTypeName, manifest, typeModule, typeModuleItemNames)
      } catch (ex) {
        this.log.error(`Error loading types: ${ex.message}`)
        reject(ex)
        return
      }
    } else {
      this.log.warn(
        `No Manifest: "${moduleName}" Is not type module. Skipping.`
      )
    }
  }

  loadQueries(baseTypeName, manifest, typeModule, typeModuleItemNames) {
    let queriesName = manifest.queries || baseTypeName + queriesSuffix
    typeModuleItemNames = this.arrayRemove(typeModuleItemNames, queriesName)
    const queries = typeModule[queriesName] || null
    if (queries) {
      this.queries = this.queries.concat(queries)
    } else {
      this.log.warn(`No Queries Found for ${baseTypeName}`)
    }
    return typeModuleItemNames
  }

  /**
   * Manifest may have a string or a list of resolvers to be loaded.
   * Handle both cases.
   * @param {*} baseTypeName
   * @param {*} manifest
   * @param {*} typeModule
   * @param {*} typeModuleItemNames
   */
  loadResolvers(baseTypeName, manifest, typeModule, typeModuleItemNames) {
    let resolvers = manifest.resolvers || baseTypeName + resolversSuffux
    if (isString(resolvers)) {
      resolvers = [resolvers]
    }
    resolvers.forEach((resolverName) => {
      const resolver = typeModule[resolverName] || null
      if (resolver) {
        typeModuleItemNames = this.arrayRemove(
          typeModuleItemNames,
          resolverName
        )
        this.resolvers = merge(this.resolvers, resolver)
      } else {
        this.log.warn(`Resolver not found ${resolverName} for ${baseTypeName}`)
      }
    })

    return typeModuleItemNames
  }

  loadManager(baseTypeName, manifest, typeModule, typeModuleItemNames) {
    const typeClass = typeModule[baseTypeName] || null
    if (!typeClass) {
      throw new Error(`No Type found for : ${baseTypeName}`)
    }
    let managerName = manifest.manager || baseTypeName + managerSuffix
    typeModuleItemNames = this.arrayRemove(typeModuleItemNames, managerName)
    const managerClass = typeModule[managerName] || null
    if (managerClass) {
      const manager = new managerClass(baseTypeName, typeClass, this.container)
      this.managers[baseTypeName] = manager
    } else {
      this.log.warn(`No Manager found for ${baseTypeName}`)
    }
    return typeModuleItemNames
  }

  /**
   * For each interface (database type) collect information that will later be used to instantiate the interface
   * and all of it's constituent APIs (one for each main Type)
   *
   * Note:  All APIs using the same DataInterface will receive typeSpec information about their own and all other sibling API Types.
   * This facilitates cross-joining across APIs sharing the same underlying database if this is necessary for the database type.
   *
   * @param {*} baseTypeName
   * @param {*} manifest
   * @param {*} typeModule
   * @param {*} typeModuleItemNames
   */
  loadAPI(baseTypeName, manifest, typeModule, typeModuleItemNames) {
    let dataConfig = manifest.data || null
    if (dataConfig) {
      const interfaceName = dataConfig.interface
      const apiName = dataConfig.api
      const apiConfig = dataConfig.config || {} // optional, per-api-type configuration
      const api = typeModule[apiName] || null
      if (api) {
        typeModuleItemNames = this.arrayRemove(typeModuleItemNames, apiName)
        const apiTypeName = dataConfig.type || baseTypeName

        let apiTypes = []
        typeModuleItemNames.forEach((tn) => {
          const t = typeModule[tn]
          apiTypes.push(t)
        })

        // save api and typeName in per-interface map list for instantiation after types are loaded.
        let interfaceEntry = this.interfaceMap[interfaceName] || {
          types: [],
          apis: [],
        }
        interfaceEntry.types = interfaceEntry.types.concat(apiTypes)
        interfaceEntry.apis.push({
          apiTypeName,
          apiName,
          api,
          config: apiConfig,
        })
        this.interfaceMap[interfaceName] = interfaceEntry
      } else {
        this.log.error(`API not found: ${apiName}`)
      }
    } else {
      this.log.warn(`No data interface config for: ${baseTypeName}`)
    }
    return typeModuleItemNames
  }

  /**
   * Create all APIs with data collected.
   */
  async createAPIs() {
    Object.keys(this.interfaceMap).forEach(async (iname) => {
      const ifdata = this.interfaceMap[iname]
      const dataInterface = this.dataInterfaces[iname]
      // prep all of the base types associated with all apis using this interface
      // This allows implementation of joins across Types within the same interface system
      await dataInterface.loadTypes(ifdata.types)
      // create and register each API using this interface.
      ifdata.apis.forEach(async (api) => {
        const apiTypeName = api.apiTypeName
        const apiClass = api.api
        const apiTypeClass = this.types[apiTypeName]
        if (isFunction(apiClass)) {
          try {
            const apiObj = await dataInterface.loadAPI(
              apiTypeName,
              apiClass,
              apiTypeClass,
              api.config
            )
            this.managers[apiTypeName].setAPI(apiObj)
          } catch (ex) {
            this.log.error(`Error loading API: ${api.apiName} ${ex}`)
          }
        } else {
          this.log.warn(`API is not a class: ${api.apiName}`)
        }
      })
    })
  }

  /**
   * If the manifest declares the types, use that list.
   * If it does not, the list in typeModuleItemNames should contain whatever is left over
   * after all other items are loaded and redacted from the list.
   * As such it should tolerate erroneous entries with warnings.
   * @param {*} baseTypeName
   * @param {*} manifest
   * @param {*} typeModule
   * @param {*} typeModuleItemNames
   */
  loadTypes(baseTypeName, manifest, typeModule, typeModuleItemNames) {
    const loadTypeNames = manifest.types || typeModuleItemNames
    // this.log.info(`Load Types for  ${baseTypeName}: ${loadTypeNames}`)
    loadTypeNames.forEach((typeName) => {
      this.loadType(typeName, typeModule)
    })
  }

  loadType(typeName, typeModule) {
    const typeClass = typeModule[typeName] || null
    if (typeClass && isFunction(typeClass)) {
      // this.log.info(`LoadType: ${typeName}`)
      if (this.types.hasOwnProperty(typeName)) {
        if (typeClass == this.types[typeName]) {
          this.log.warn(
            `Duplicate Type: ${typeName} is already loaded, Skipping`
          )
        } else {
          this.log.error(
            `Type named ${typeName} is already loaded but is different! `
          )
        }
      } else {
        try {
          const typeObj = new typeClass()
          this.types[typeName] = typeClass // save the Class, not the obj in types
          const schema = typeObj.toGraphQLSchema()
          this.schemas = this.schemas.concat(schema)
        } catch (ex) {
          this.log.warn(`Error loading Type: ${ex}`)
        }
      }
    } else {
      this.log.error(`loadType not a function: ${typeName}`)
    }
  }

  arrayRemove(arr, itemName) {
    const ix = arr.indexOf(itemName)
    if (ix >= 0) {
      arr = concat(arr.slice(0, ix), arr.slice(ix + 1))
    }
    return arr
  }

  firstUpperCase(str) {
    if (typeof str === "string") {
      return str[0].toUpperCase() + str.slice(1)
    }
    return null
  }
  getTypes() {
    return this.types
  }

  getTypeDefs() {
    return this.queries.concat(this.schemas)
  }

  getResolvers() {
    return this.resolvers
  }

  getManagers() {
    return new ManagersMap(this.managers)
  }
}
